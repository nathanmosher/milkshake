module.exports = {
  bar: {
    route: '/bar',
    title: 'bar',
    controller: '/bar/controller.js',
    template: '/bar/view.swig'
  },
  barBarz: {
    route: '/bar/barz',
    title: 'bar | barz',
    controller: '/bar/barz/controller.js',
    template: '/bar/barz/view.swig'
  },
  barBazz: {
    route: '/bar/bazz',
    title: 'bar | bazz',
    controller: '/bar/bazz/controller.js',
    template: '/bar/bazz/view.swig'
  },
  barFoobz: {
    route: '/bar/foobz',
    title: 'bar | foobz',
    controller: '/bar/foobz/controller.js',
    template: '/bar/foobz/view.swig'
  }
}
