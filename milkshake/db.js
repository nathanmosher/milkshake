'use strict';

var Promise = require('bluebird');
var Datastore = require('nedb');
var db = Promise.promisifyAll(new Datastore());

module.exports = db;
