#!/usr/bin/env node

'use strict';

var Promise = require('bluebird');

var glob = Promise.promisify(require('glob'));
var rimraf = Promise.promisify(require('rimraf'));
var mkdirp = Promise.promisify(require("mkdirp"));
var fs = Promise.promisifyAll(require('fs'));
var copy = Promise.promisify(require('directory-copy'));

var db = require('./db');
var swig = require('swig');
var path = require('path');
var _ = require('lodash');
var nedb = require("nedb");
var minify = require('html-minifier').minify;
var moment = require('moment');
var program = require('commander');

// // standard LiveReload port
// var port = 35729;
// // tinylr(opts) => new tinylr.Server(opts);
// tinylr().listen(port, function() {
//   console.log('... live reload listening on %s ...', port);
// });


var rootPath = path.join(__dirname, '..');
var srcPath = path.join(rootPath, 'src');
var distPath = path.join(rootPath, 'dist');

var routes = require(path.join(srcPath, 'routes'));

swig.setDefaults({ cache: false });

var Generator = function () {};

Generator.prototype.generateRoute = function (route, routes) {

  var controller = require(path.join(srcPath, route.controller));

  // cloned so each page gets their own context
  controller.global = _.clone(require(path.join(srcPath, 'global')));

  controller.global.router = {
    routes: routes,
    current: route
  };

  return Promise.props(controller).then(function (data) {

    var templatePath = path.join(srcPath, route.template);

    swig.invalidateCache();

    var rendered = swig.renderFile(templatePath, data);

    var minified = minify(rendered, {
      collapseWhitespace: true,
      conservativeCollapse: true,
      preserveLineBreaks: true,
      useShortDoctype: true
    });

    var targetPath = path.join(route.route, 'index.html');

    return {
      id: targetPath,
      content: minified
    }
  });
};

Generator.prototype.cleanDist = function (route) {
  return rimraf(distPath).then(function () {
    var staticDistPath = path.join(distPath, 'static');
    return mkdirp(staticDistPath).then(function () {
      console.log('dist directory cleaned')
    })
  });
};

Generator.prototype.saveStatic = function () {
  var self = this;

  var pattern = 'static/**/*';
  var opts = {
    cwd: srcPath,
    nodir: true
  };
  return glob(pattern, opts).then(function (files) {
    return _.map(files, function (file) {
      return self.saveStaticFileToDb('/' + file);
    });
  });
  // .then(function () {
  //     console.log('saved static directory to db');
  // });;
};

Generator.prototype.copyStatic = function () {
  return copy({
    src: path.join(srcPath, 'static'),
    dest: path.join(distPath, 'static')
  }).then(function () {
    console.log('copied static directory to dist');
  });
};

Generator.prototype.generate = function () {

  var self = this;
  self.startTime = moment();

  return self.cleanDist().then(function () {
    if (global.program.memory) {
      return self.saveStatic();
    } else {
      return self.copyStatic();
    }
  }).then(function() {
    return _.map(routes, function (route, id, routes) {
      route.id = id;
      return self.generateRoute(route, routes);
    });
  }).map(function(page) {
    if (global.program.memory) {
      return self.savePageToDb(page);
    } else {
      return self.savePageToFs(page);
    }
  }).then(function (args) {
    self.endTime = moment();
    var duration = self.endTime.diff(self.startTime);
    console.log('generated %s pages in %s ms', args.length, duration);

    return args;
  });

};

Generator.prototype.savePageToDb = function (page) {
  return db.insertAsync(page).then(function (doc) {
    console.log('saved %s to db', doc.id);
    return page;
  });
};

Generator.prototype.saveStaticFileToDb = function (file) {
  var srcStaticPath = path.join(srcPath, file);
  fs.readFileAsync(srcStaticPath, 'utf8').then(function (data) {
    return db.insertAsync({id:file, content: data}).then(function (doc) {
      console.log('saved %s to db', doc.id);
      return doc;
    });
  })

};

Generator.prototype.savePageToFs = function (page) {

  var targetFile = path.join(distPath, page.id);
  var targetDir = path.dirname(targetFile);
  return mkdirp(targetDir).then(function(result) {

    if(result != null) {
      console.log('created directory %s', targetDir);
    } else {
      console.log('directory %s already created', targetDir);
    }

    return page;
  }).then(function (page) {

    return fs.writeFileAsync(targetFile, page.content).then(function() {
      console.log('file written:', page.target);
      return page;
    });
  });
};

module.exports = exports = new Generator();
