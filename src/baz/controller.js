'use strict';

var Promise = require('bluebird');
var moment = require('moment');
var request = Promise.promisify(require('request'));

var getArticles = function () {
  var start = moment();
  return request('http://www.filltext.com/?rows=10&title={lorem|2}&text={lorem|50}')
    .then(function(data) {
      var end = moment();
      var articles = JSON.parse(data[0].body);
      console.log('requested data from external api in %s ms', end.diff(start));
      return articles;
    });
}

module.exports = {
  title: 'baz page',
  articles: getArticles()
};
