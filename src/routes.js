'use strict';

var _ = require('lodash');

var routes = {
  home: {
    route: '/',
    title: 'home',
    controller: '/controller.js',
    template: '/view.swig'
  },
  baz: {
    route: '/baz',
    title: 'baz',
    controller: '/baz/controller.js',
    template: '/baz/view.swig'
  },
  foo: {
    route: '/foo',
    title: 'foo',
    controller: '/foo/controller.js',
    template: '/foo/view.swig'
  }
};

module.exports = _.extend(routes, require('./bar/routes'));
