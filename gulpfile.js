var gulp = require('gulp');
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var util = require('gulp-util');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');
var path = require('path');
var _ = require('lodash');
require('colors');

var mode = !util.env.dev ? 'dev' : 'prod';

/**
 * browserify build
 */

var browswerifyOpts = {
  entries: ['./src/js/main.js'],
  debug: true
};
var browswerifyOpts = _.assign({}, watchify.args, browswerifyOpts);
var b = watchify(browserify(browswerifyOpts));

gulp.task('js', bundle); // so you can run `gulp js` to build the file
 // on any dep update, runs the bundler
b.on('log', util.log); // output build logs to terminal

function bundle() {
  return b.bundle()
    // log errors if they happen
    .on('error', util.log.bind(util, 'Browserify Error'))
    .pipe(source('main.js'))
    // optional, remove if you don't need to buffer file contents
    .pipe(buffer())
    // optional, remove if you dont want sourcemaps
    .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
       // Add transformation tasks to the pipeline here.
       .pipe(uglify())
    .pipe(sourcemaps.write('./')) // writes .map file
    .pipe(gulp.dest('./src/static/js'))
    .pipe(livereload());
}


/**
 * scss build
 */
var plumberOpts = {
  errorHandler: function (err) {
    console.log('Plumber Error:', Object.keys(err));
  }
};
var sassOpts = {
  outputStyle: !!util.env.dev ? 'compressed' : 'compact',
  sourceMap: !!util.env.dev ? true : false,
  omitSourceMapUrl: !!util.env.dev ? false : true,
  onError: function (error) {
    console.log(
      '%s %s \nfile: %s\nline: %s\ncol:  %s',
      'Sass:'.red.bold,
      error.message.red,
      error.file.cyan,
      error.line.toString().cyan,
      error.column.toString().cyan);
  },
  onSuccess: function(result) {
    // result is an object: v2 change
    console.log('%s %s\nfile: %s\nms:   %s',
      'Sass:'.green.bold,
      'successfully compiled'.green,
      result.stats.entry.cyan,
      result.stats.duration.toString().cyan)
  }
};
var prefixerOpts = {
  browsers: ['last 3 versions'],
  cascade: false
};

gulp.task('scss', function() {

  var src = 'src/scss/main.scss';
  var dest = 'src/static/css/';

  console.log('Compiling %s to %s in %s mode.',
      src.cyan, dest.magenta, mode.blue);

  // Compress output and remove source maps with --dev flag
  if (mode == 'prod') {
    gulp.src(src)
      .pipe(sass(sassOpts))
      .pipe(autoprefixer(prefixerOpts))
      .pipe(gulp.dest(dest));
  } else {
    gulp.src(src)
      .pipe(plumber(plumberOpts))
      .pipe(sourcemaps.init())
        .pipe(sass(sassOpts))
        .pipe(autoprefixer(prefixerOpts))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(dest))
      .pipe(livereload());
  }
});


/**
 *  watch build
 */

gulp.task('watch', function() {
  livereload.listen();

  // build the bundle
  bundle();
  // update bundle
  b.on('update', bundle);
  gulp.watch('src/scss/**/*', ['scss']);
});

