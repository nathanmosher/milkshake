#!/usr/bin/env node

'use strict';

var express = require('express');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var path = require('path');

var generator = require('./generator');
var db = require('./db');


var app = express();

app.use(compression());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cookieParser());
app.use(morgan('dev'));
app.use(errorHandler());

// app.use(require('connect-livereload')({
//   port: 35729
// }));

app.use('/', express.static('dist', { maxAge: 1 }));

if (global.program.memory) {
  // serve from db
  app.get('*', function (req, res) {

    var ext = path.extname(req.url);

    var id = (ext == '') ? path.join(req.url, 'index.html') : req.url;

    db.findOneAsync({ id: id }).then(function (file) {
      console.log('found file %s in db', file.id);
      res.type(path.extname(file.id)).status(200).send(file.content);
    }).catch(function () {
      console.log('couldnt find file %s in db', id);
      res.status(404).send('could not find '+ req.url);
    });
  });
}

generator.generate().then(function () {

  var mode = global.program.memory ? 'memory' : 'disk';

  app.listen(program.port, function () {
    console.log('express server listening on port %s, serving site from %s',
        program.port, mode);
  });
});


module.exports = exports = app;
