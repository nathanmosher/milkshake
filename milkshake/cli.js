#!/usr/bin/env node

var program = require('commander');
var generator = require('./generator');
var VERSION = require('../package.json').version;

program.version(VERSION);

program
  .option('-d, --disk', 'generate/serve the site from disk')
  .option('-m, --memory', 'generate/serve the site from memory');

program.memory = program.memory || false;
program.disk = program.disk || true;

global.program = program;

program.command('serve')
  .description('serve that site w express')
  .option('-p, --port [n]', 'port to start server on', parseInt)
  .action(function() {
    program.port = process.env.PORT || program.port || 9000;
    var server = require('./serve');
  });

program.command('generate [description]')
  .description('generate yr site')
  .action(function() {
    generator.generate();
  });

program.parse(process.argv);


